<?php include_once 'inc/top.php'; ?>

<div class="container">
    <br>
    <h1>Lisää kirjoitus</h1>
    <br>
    
    <form action="<?php echo $_SERVER['PHP_SELF']?>" method="post">
        <div class="form-group">
            <label>Otsikko</label>
            <input type="text" class="form-control" name="otsikko" placeholder="Otsikko tähän">
        </div>
        <div class="form-group">
            <label>Teksti</label>
            <textarea class="form-control" rows="5" name="teksti" placeholder="Teksti tänne..."></textarea>
        </div>
        <button class="btn btn-primary" type="submit">Tallenna</button> &nbsp;
        <button class="btn btn-default" type="button" onclick="window.location='index.php';">Peruuta</button>
    </form>
    <?php saveDatabaseData(); ?>
</div>

<?php include_once 'inc/bottom.php'; ?>