<?php

session_start();


//
function getDatabaseData() {
    $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
    $tietokanta ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    try {
        //$sql = 'SELECT * FROM kirjoitus ORDER BY paivays desc';
        $sql = 'SELECT *, kirjoitus.id as id FROM kirjoitus INNER JOIN kayttaja ON kirjoitus.kayttaja_id = kayttaja.id ORDER BY paivays desc';
        $kysely = $tietokanta ->query($sql);
        
        if($kysely) {
            while ($tietue = $kysely ->fetch()) {
                $db = $tietue['paivays'];
                $timestamp = strtotime($db);
                print '<div class="row">';
                print '<div id="mydiv" class="col-sm-10">';
                print '<p>' . date("d.m.Y H.i", $timestamp) . ' by ' . $tietue['tunnus'] . '</p>';
                print '<p> &nbsp; <b><a href="blogi.php?kirjoitus=' . $tietue['id'] . '">' . $tietue['otsikko'] . '</a>&nbsp;&nbsp;&nbsp;';
                print '<a href="poista.php?ki_id=' . $tietue['id'] . '"><span class="glyphicon glyphicon-trash"></span></a></b><p>';
                //print '<hr>';
                print '</div>';
                print '</div>';
                print '<br>';
            }
        }
    } catch (PDOException $pdoex) {
        print '<p class="e_msg">Kirjoitusten hakeminen epäonnistui.' . $pdoex -> getMessage() . '</p>';
    }
}


//
function commentData() {
    $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
    $tietokanta ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $kirjoitus_id = $_GET['kirjoitus'];

    try {
            $sql = 'SELECT *, kirjoitus.id as id FROM kirjoitus INNER JOIN kayttaja ON kirjoitus.kayttaja_id = kayttaja.id WHERE kirjoitus.id = ' . $kirjoitus_id;
            $kysely = $tietokanta ->query($sql);

            if($kysely) {
                $tietue = $kysely ->fetch();
                $db = $tietue['paivays'];
                $timestamp = strtotime($db);
                print '<h3><b>' . $tietue['otsikko'] . '</b></h3>';
                print '<p>' . date("d.m.Y H.i", $timestamp) . ' by ' . $tietue['tunnus'] . '</p>';
                print '<p>' . $tietue['teksti'] . '</p>';
            }
        } catch (PDOException $pdoex) {
            print '<p class="e_msg">Tekstin hakeminen epäonnistui.' . $pdoex -> getMessage() . '</p>';
        }


    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if(isset($_SESSION['login'])) {
            try {
                $kommentti = filter_input(INPUT_POST, 'kommentti', FILTER_SANITIZE_STRING);
                $kirjoitus_id = filter_input(INPUT_POST, 'kirjoitus_id', FILTER_SANITIZE_NUMBER_INT);

                $kysely = $tietokanta ->prepare("INSERT INTO kommentti(teksti, kirjoitus_id, kayttaja_id) VALUES (:kommentti, :kirjoitus_id, :kayttaja_id)");

                $kysely -> bindValue(':kommentti', $kommentti, PDO::PARAM_STR);
                $kysely -> bindValue(':kirjoitus_id', $kirjoitus_id, PDO::PARAM_INT);
                $kysely -> bindValue(':kayttaja_id', $_SESSION['kayttaja_id'], PDO::PARAM_INT);

                $kysely ->execute();
                header("Location: blogi.php?kirjoitus=$kirjoitus_id");
                exit;
            } catch (PDOException $pdoex) {
                    print '<p class="e_msg">Kommentin tallentaminen epäonnistui' . $pdoex -> getMessage() . '</p>';
            }
        } else {
            print("<div class='alert alert-warning'><strong>Et ole kirjautunut!</strong> Kirjaudu sisään kommentointia varten.</div>");
        }
    }
    ?>
    <p><b>Kommentit</b></p>
        <form id="lisaa_kommentti" method="post" action="<?php if(isset($_SESSION['login'])) { print($_SERVER['PHP_SELF']); }?>">
            <input type="hidden" name="kirjoitus_id" value="<?php print $tietue['id']; ?>">
            <textarea class="textarea" name="kommentti" id="kommentti"></textarea>
            <p><button class="btn btn-primary">Tallenna</button><p>
        </form>
    <?php
    
    try {
        $sql = 'SELECT *, kommentti.id as id FROM kommentti INNER JOIN kayttaja ON kommentti.kayttaja_id = kayttaja.id WHERE kommentti.kirjoitus_id = ' . $kirjoitus_id . ' ORDER BY paivays desc';
        $kysely = $tietokanta ->query($sql);
        
        if($kysely) {
            while ($tietue = $kysely ->fetch()) {
                $db = $tietue['paivays'];
                $timestamp = strtotime($db);
                print '<ul>';
                print '<li>' . $tietue['teksti'] . '&nbsp;' . date("d.m.Y H.i", $timestamp) . ' by ' . $tietue['tunnus'] . ' &nbsp;<a href="poista.php?ko_id=' . $tietue['id'] . '"><span class="glyphicon glyphicon-trash"></span></a></li>';
                print '</ul>';
            }
        }
    } catch (PDOException $pdoex) {
        print '<p class="e_msg">Tekstin hakeminen epäonnistui.' . $pdoex -> getMessage() . '</p>';
    }
}


//
function saveDatabaseData() {
$tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
$tietokanta ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$id = 1;
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    try {
        $id = $_SESSION['kayttaja_id'];
        $otsikko = filter_input(INPUT_POST, 'otsikko', FILTER_SANITIZE_STRING);
        $teksti = filter_input(INPUT_POST, 'teksti', FILTER_SANITIZE_STRING);

        $kysely = $tietokanta ->prepare("INSERT INTO kirjoitus(otsikko, teksti, kayttaja_id) VALUES (:otsikko, :teksti, $id)");

        $kysely -> bindValue(':otsikko', $otsikko, PDO::PARAM_STR);
        $kysely -> bindValue(':teksti', $teksti, PDO::PARAM_STR);

        if($kysely ->execute()) {
            print('<p class="s_msg">Uusi kirjoitus lisätty</p>');
            //print('<p class="s_msg"><a href="index.php">Takaisin etusivulle</a></p>');
        } else {
            print '<p class="e_msg">';
            print_r($tietokanta -> errorInfo());
            print '</p>';
        }
        } catch (PDOException $pdoex) {
                print '<p class="e_msg">Kirjoituksen tallentaminen epäonnistui' . $pdoex -> getMessage() . '</p>';
        }
    }
}


//
function deleteDatabaseData() {
    
    try {
        $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
        $tietokanta ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        if(isset($_SESSION['login'])) {
            if(isset($_GET['ki_id'])) {
                $ki_id = filter_input(INPUT_GET, 'ki_id', FILTER_SANITIZE_NUMBER_INT);
                $kysely = $tietokanta ->prepare("DELETE FROM kommentti WHERE kirjoitus_id=:ki_id");
                $kysely2 = $tietokanta ->prepare("DELETE FROM kirjoitus WHERE id=:ki_id");

                $kysely -> bindValue(':ki_id', $ki_id, PDO::PARAM_INT);
                $kysely2 -> bindValue(':ki_id', $ki_id, PDO::PARAM_INT);

                if($kysely->execute() && $kysely2->execute()) {
                    print ('<p class="s_msg">Kirjoitus poistettu.</p>');
                } else {
                    print '<p class="e_msg">';
                    print_r($tietokanta -> errorInfo());
                    print '</p>';
                }
            }

            if(isset($_GET['ko_id'])) {
                $ko_id = filter_input(INPUT_GET, 'ko_id', FILTER_SANITIZE_NUMBER_INT);
                $kysely = $tietokanta ->prepare("DELETE FROM kommentti WHERE id=:ko_id");

                $kysely -> bindValue(':ko_id', $ko_id, PDO::PARAM_INT);

                if($kysely->execute()) {
                    print ('<p class="s_msg">Kommentti poistettu.</p>');
                } else {
                    print '<p class="e_msg">';
                    print_r($tietokanta -> errorInfo());
                    print '</p>';
                }
            }
        } else {
            print("<div class='alert alert-warning'><strong>Et ole kirjautunut!</strong> Kirjaudu sisään poistaaksesi tiedot.</div>");
        }

        print("<a class='s_msg' href='index.php'>Takaisin etusivulle.</a>");

    } catch (PDOException $pdoex) {
            print '<p class="e_msg">Tietokannan avaus epäonnistui.' . $pdoex -> getMessage() . '</p>';
    }
}


//
function loginCheck () {
    $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
    $tietokanta ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if ($tietokanta != null) {
            try {
                $tunnus = filter_input(INPUT_POST, 'tunnus', FILTER_SANITIZE_STRING);
                $salasana = md5(filter_input(INPUT_POST, 'salasana', FILTER_SANITIZE_STRING));
                
                $sql = "SELECT * FROM kayttaja WHERE tunnus='$tunnus' AND salasana='$salasana'";
                
                $kysely = $tietokanta ->query($sql);
                
                if($kysely ->rowCount() === 1) {
                    $tietue = $kysely ->fetch();
                    $_SESSION['login'] = true;
                    $_SESSION['kayttaja_id'] = $tietue['id'];
                    $_SESSION['kayttaja_nimi'] = $tietue['tunnus'];
                    header('Location: index.php');
                } else {
                    print '<p class="s_msg">Väärä tunnus tai salasana.';
                } 
            } catch (PDOException $pdoex) {
                print '<p class="e_msg">Käyttäjän tietojen hakeminen epäonnistui.' . $pdoex -> getMessage() . '</p>';
            }
        }
    }
}
