<?php
include_once 'inc/top.php';

unset($_SESSION['login']);
unset($_SESSION['kayttaja_id']);
session_destroy();

header('Location: index.php');

include_once 'inc/bottom.php'; ?>